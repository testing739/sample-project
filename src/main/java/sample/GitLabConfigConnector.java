package sample;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.client.UnknownHttpStatusCodeException;

@Service
@Slf4j
public class GitLabConfigConnector {
	@Autowired
	RestTemplate restTemplate;
	
	public String getContents() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("PRIVATE-TOKEN", "glpat-XXX");
		HttpEntity<Void> requestEntity = new HttpEntity<>(headers);
		
		String url = "https://gitlab.com/api/v4/projects/36325066/repository/files/src%2Fmain%2Fresources%2Fdata.txt/raw?ref=main";
		
		log.info("Calling GitLab API at [{}]", url);
		
		try {
			return restTemplate.exchange(url, HttpMethod.GET, requestEntity, String.class).getBody();
		} catch (HttpClientErrorException | HttpServerErrorException | UnknownHttpStatusCodeException e) {
			log.error("Error calling GitLab - {}", e.getMessage());
		}
		
		return null;
	}
}
